﻿using System;
using UnityEngine;

public abstract class DataScriptable : ScriptableObject
{
    [SerializeField]
    protected string _id;

    [SerializeField]
    protected string _name;

    public string Id
    {
        get { return _id; }
        set { _id = value; }
    }

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public void Awake()
    {
        _id = Guid.NewGuid().ToString();
    } 
}
