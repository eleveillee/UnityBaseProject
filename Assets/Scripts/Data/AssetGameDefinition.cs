﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;

public class AssetGameDefinition : ScriptableObject
{
    /*
    [Header("Player")]
    public DataPlayer Player;
    public List<BasePlayerController> FlyControllers;
    public List<BasePlayerController> LaunchControllers;
    public List<float> ComboPoints;

    [Header("World")]
    [Tooltip("World limits from wall to wall")]
    public Vector2 WorldX;
    [Tooltip("World limits from starting platform to top")]
    public Vector2 WorldY;
    
    [Header("GameModes")]
    public List<DataGameMode> GameModes;
    public List<DataCircle> Circles;
    public DataCircle TutoCircle;
    public DataCircle DebugCircle;

    [Header("Spawnables")]
    public List<DataMonster> Monsters;
    public List<DataPowerUp> PowerUps;
    public List<DataUpgrade> Upgrades;
    public List<DataFeature> Features;

    public List<DataQuest> MainQuests;

    //Helpers
    //private List<DataPowerUp> _powerUpsBubbleCache = null; public List<DataPowerUp> PowerUps_Bubbles { get { Debug.Log("Null : " + (_powerUpsBubbleCache == null)); if(_powerUpsBubbleCache == null || _powerUpsBubbleCache.Count <= 0) )
    //        { _powerUpsBubbleCache = PowerUps.FindAll(x => x.IsBubble); Debug.Log("Count : " + PowerUps.Count); }  return _powerUpsBubbleCache; } }
    private List<DataPowerUp> _powerUpsBubbleCache; public List<DataPowerUp> PowerUps_Bubbles { get { if (_powerUpsBubbleCache == null || _powerUpsBubbleCache.Count <= 0) _powerUpsBubbleCache = PowerUps.FindAll(x => x.IsBubble); return _powerUpsBubbleCache; } }
    private List<DataPowerUp> _powerUpsLootsCache; public List<DataPowerUp> PowerUps_Loots { get { if (_powerUpsLootsCache == null || _powerUpsLootsCache.Count <= 0) _powerUpsLootsCache = PowerUps.FindAll(x => x.IsLoot); return _powerUpsLootsCache; } }
    */
}

