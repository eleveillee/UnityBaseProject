﻿using System;
using UnityEngine;

public class DataMonster : ScriptableObject
{
    public string Name;
    public int Hp = 1;
    public float ComboPoints = 1f;
    //public Monster Prefab;
    public int GoldValue;
    public int SizeCells = 1;
    //public Sprite _icon;
}
