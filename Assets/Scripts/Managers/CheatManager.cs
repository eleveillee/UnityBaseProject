using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatManager : MonoBehaviour {

    public static CheatManager Instance;
    bool _isDebugVerbose = true;
    
    /*
    public bool IsNoSlideCheat_F1;
    public bool IsUseNoEnergy = true;
    public bool IsGodMode_F4;
    public bool IsAutoWallBounce;
    public bool IsShowingDebugLogs_F9;
    public bool IsShowingFloatingLogs_F10;
    public bool IsUsingIconForMonsters_F11;

    public bool IsSkipLauncher;
    */

    void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        //DoCheat();
    }

    /* // F#
    F1  : NoSlide
    F2  : Skip World/Boss
    F3  : Kill Em' All by Metallica
    F4  : GodMode = No Death
    F5  : AddHp
    F6  : AddSpeed
    F7  : AddHeight
    F8  : AddEnergy
    F9  : Display Extra Debug On Screen
    F10 : Display FloatingTexts
    F11 : Toggle Icon visuals

    // # Tutorials
    1 : Skip All Tutorials
    2 : Skip CurrentTuto
    3 : Skip CurrentStep
    4 : Return To PreviousStep(Unstable)

    // GameFlow
    E : EndGame
    R : Restart

    // Spawn
    P : Spawn PowerUps
    L : Spawn Loots
    F : Spawn Feature
    M : Spawn Monster

    // Stats
    G : AddGold
    C : AddCombo    
    -/+ : AddHP (NumPad) 

    // Visual
    X : View Grid
    */
    #region Cheats

    /*
    // F1 NoSlide
    public void Cheat_F1NoSlide(bool isOn)
    {
        IsNoSlideCheat_F1 = isOn;
        //if (_isDebugVerbose) Debug.Log("[CHEAT] NoSlide : " + (IsNoSlideCheat_F1 ? "ON" : "OFF"));
    }
    
    //F2 SkipCurrentSection
    void Cheat_F2SkipCurrentSection()
    {
        if (UnityEngine.XR.WSA.WorldManager.Instance == null || UnityEngine.XR.WSA.WorldManager.Instance.CircleInstance == null)
            return;

        if (!UnityEngine.XR.WSA.WorldManager.Instance.CircleInstance.IsInBossState)
        {
            MonsterManager.Instance.CleanUp();
            UnityEngine.XR.WSA.WorldManager.Instance.AttainHeight(true);
        }
        else
        {

            WaveManager.Instance.ToggleSpawning(false);
            WaveManager.Instance.CleanUp(); ;
            MonsterManager.Instance.CleanUp();
            UnityEngine.XR.WSA.WorldManager.Instance.OnBossKilled();
        }


        if (_isDebugVerbose) Debug.Log("[CHEAT] Current Section Skipped");
    }

    //F3 KillAllMonsters
    void Cheat_F3KillAllMonsters()
    {
        if (_isDebugVerbose) Debug.Log("[CHEAT] Killed " + MonsterManager.Instance.Monsters.Count + " monsters");
        MonsterManager.Instance.Monsters.ForEach((x) => x.AddHp(-1000000));
    }

    //F4 GodMode
    public void Cheat_F4GodMode(bool? isOn = null)
    {
        if (isOn == null)
            IsGodMode_F4 = !IsUseNoEnergy;
        else
            IsGodMode_F4 = (bool)isOn;

        if (_isDebugVerbose) Debug.Log("[CHEAT] GodMode : " + (IsGodMode_F4 ? "ON" : "OFF"));
    }
    
    public void Cheat_NoEnergy(bool? isOn = null)
    {
        if (isOn == null)
            IsUseNoEnergy = !IsUseNoEnergy;
        else
            IsUseNoEnergy = (bool)isOn;

        if (_isDebugVerbose) Debug.Log("[CHEAT] NoEnergy : " + (IsUseNoEnergy ? "ON" : "OFF"));
    }

    public void Cheat_AutoBounce(bool? isOn = null)
    {
        if (isOn == null)
            IsAutoWallBounce = !IsUseNoEnergy;
        else
            IsAutoWallBounce = (bool)isOn;

        if (_isDebugVerbose) Debug.Log("[CHEAT] AutoBounce : " + (IsAutoWallBounce ? "ON" : "OFF"));
    }

    //F9 Show Debug Logs
    public void Cheat_F9DebugLogs(bool? isOn = null)
    {
        if (isOn == null)
            IsShowingDebugLogs_F9 = !IsUseNoEnergy;
        else
            IsShowingDebugLogs_F9 = (bool)isOn;
        UIManager.Instance.Overlay.ShowDebug(IsShowingDebugLogs_F9);
        if (_isDebugVerbose) Debug.Log("[CHEAT] Show Debug Logs : " + (IsShowingDebugLogs_F9 ? "ON" : "OFF"));
    }

    //F10 Show Debug Floating Lggs
    public void Cheat_F10DebugFloatingLogs(bool? isOn = null)
    {
        if (isOn == null)
            IsShowingFloatingLogs_F10 = !IsUseNoEnergy;
        else
            IsShowingFloatingLogs_F10 = (bool)isOn;

        if (_isDebugVerbose) Debug.Log("[CHEAT] Show Debug Logs : " + (IsShowingFloatingLogs_F10 ? "ON" : "OFF"));
    }

    //F11 Show Debug Floating Lggs
    public void Cheat_F11UseIconForMonsters(bool isUsingIconForMonsters)
    {
        IsUsingIconForMonsters_F11 = isUsingIconForMonsters;
        MonsterManager.Instance.Monsters.ForEach(x => x.Controller.SetIconVsFullBody());
        if (_isDebugVerbose) Debug.Log("[CHEAT] Use Icon For Monsters : " + (IsUsingIconForMonsters_F11 ? "ON" : "OFF"));
    }

    


    //1 SkipAllTutorials
    public void Cheat_1SkipAllTutorials()
    {
        TutorialManager.Instance.Debug_SkipAllTutorials();
        if (_isDebugVerbose) Debug.Log("[CHEAT] SkipAllTutorials");
    }

    //2 SkipAllTutorials
    public void Cheat_2CompleteTutorial()
    {
        TutorialManager.Instance.Debug_CompleteTutorial();
        if (_isDebugVerbose) Debug.Log("[CHEAT] Complete Tutorial");
    }

    //3 SkipAllTutorials
    public void Cheat_3CompleteStep()
    {
        TutorialManager.Instance.Debug_CompleteStep();
        if (_isDebugVerbose) Debug.Log("[CHEAT] CompleteStep");
    }

    //4 SkipAllTutorials
    public void Cheat_4ReturnToPreviousStep()
    {
        TutorialManager.Instance.Debug_ReturnToPreviousStep();
        if (_isDebugVerbose) Debug.Log("[CHEAT] ReturnToPreviousStep");
    }

    public void Cheat_AddGold()
    {
        int goldAmount = 100;
        if (Player.Instance != null)
            LevelTracker.Instance.AddGold(goldAmount, true);
        else
            Profile.Instance.Data.Inventory.Currency.Add(goldAmount);

        if (_isDebugVerbose) Debug.Log("[CHEAT] Gold +100");
    }

    public void Cheat_AddCombo()
    {
        if (Player.Instance != null)
            Player.Instance.AddCombo(1, 0f, true);

        if (_isDebugVerbose) Debug.Log("[CHEAT] Combo +1");
    }

    public void Cheat_SpawnBubble()
    {
        PowerUp pup = PowerUpManager.Instance.SpawnPowerUp(PowerUpManager.Instance.GetRandomPowerUp(PowerUpType.Bubble), PowerUpManager.Instance.GetRandomPosition());
        if (_isDebugVerbose) Debug.Log("[CHEAT] Spawned pwrUp_Bubble : " + pup.Data.Effect);
    }

    public void Cheat_SpawnLoot()
    {
        PowerUp pup = PowerUpManager.Instance.SpawnPowerUp(PowerUpManager.Instance.GetRandomPowerUp(PowerUpType.Loot), PowerUpManager.Instance.GetRandomPosition());
        if (_isDebugVerbose) Debug.Log("[CHEAT] Spawned pwrUp_Loot: " + pup.Data.Effect);
    }

    public void Cheat_SpawnMonster()
    {
        Monster monster = MonsterManager.Instance.SpawnRandomCurrentMonster(true);
        if (_isDebugVerbose) Debug.Log("[CHEAT] Spawned Monster: " + monster.Data.Name + " at (" + monster.transform.localPosition.x.ToString("F1") + "," + monster.transform.localPosition.y.ToString("F1") +  ")");
    }

    public void Cheat_SpawnFeature()
    {
        //FeatureManager.Instance.SpawnFeature(GameDef.Instance.Data.Features.GetRandomValue(), FeatureManager.Instance.GetRandomPosition());
        FeatureManager.Instance.SpawnRandomFeature();
        if (_isDebugVerbose) Debug.Log("[CHEAT] Spawned Feature");
    }

    public void Cheat_ToggleGrid()
    {
        DebugManager.Instance.ToggleGrid();
    }
    */
    #endregion
    /*
    void DoCheat()
    {
        #region F1-12
        if (Input.GetKeyDown(KeyCode.F1))
        {
            Cheat_F1NoSlide(!IsNoSlideCheat_F1);
        }
        else if (Input.GetKeyDown(KeyCode.F2))
        {
            Cheat_F2SkipCurrentSection();
        }
        else if (Input.GetKeyDown(KeyCode.F3))
        {
            Cheat_F3KillAllMonsters();
        }
        else if (Input.GetKeyDown(KeyCode.F4))
        {
            Cheat_F4GodMode(!IsGodMode_F4);
        }
        else if (Input.GetKey(KeyCode.F5))
        {
            Player.Instance.AddHp(1);
            if (_isDebugVerbose) Debug.Log("[CHEAT] HP +1");
        }
        else if (Input.GetKey(KeyCode.F6))
        {
            if(Player.Instance != null)
                Player.Instance.AddSpeed(1f);
            if (_isDebugVerbose) Debug.Log("[CHEAT] Speed++");
        }
        else if (Input.GetKey(KeyCode.F7))
        {
            if (LevelTracker.Instance != null && LevelTracker.Instance.Data != null)
                UnityEngine.XR.WSA.WorldManager.Instance.CircleInstance.Height += 1f;
            if (_isDebugVerbose) Debug.Log("[CHEAT] Height++");
        }
        else if (Input.GetKey(KeyCode.F8))
        {
            if (Player.Instance != null)
                Player.Instance.AddEnergy(10f);
            if (_isDebugVerbose) Debug.Log("[CHEAT] Energy +10");
        }
        if (Input.GetKeyDown(KeyCode.F9))
        {
            Cheat_F9DebugLogs(!IsShowingDebugLogs_F9);
            if (_isDebugVerbose) Debug.Log("[CHEAT] Show Debug Logs : " + IsShowingDebugLogs_F9);
        }
        else if (Input.GetKeyDown(KeyCode.F10))
        {
            Cheat_F10DebugFloatingLogs(!IsShowingFloatingLogs_F10);
            if (_isDebugVerbose) Debug.Log("[CHEAT] Show Debug Floating Logs : " + IsShowingFloatingLogs_F10);
        }
        else if (Input.GetKeyDown(KeyCode.F11))
        {
            Cheat_F11UseIconForMonsters(!IsUsingIconForMonsters_F11);
            //if (_isDebugVerbose) Debug.Log("[CHEAT] Show Debug Floating Logs : " + IsUsingIconForMonsters_F11);
        }
        else if (Input.GetKeyDown(KeyCode.F12))
        {

            //if (_isDebugVerbose) Debug.Log("[CHEAT]            //Player.Instance.GetComponent<PlayerView>().IsUsingSword = !Player.Instance.GetComponent<PlayerView>().IsUsingSword; Toggle sword : " + Player.Instance.GetComponent<PlayerView>().IsUsingSword);
        }
        #endregion

        #region 1-9 (Tutorials)
        if (Input.GetKeyDown(KeyCode.Alpha1) && UIManager.Instance.CurrentPanel == null) //NullCheck current panel to avoid conflict with GameWorldSelect alpha inputs
        {
            Cheat_1SkipAllTutorials();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) && UIManager.Instance.CurrentPanel == null)
        {
            Cheat_2CompleteTutorial();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3) && UIManager.Instance.CurrentPanel == null)
        {
            Cheat_3CompleteStep();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4) && UIManager.Instance.CurrentPanel == null)
        {
            Cheat_4ReturnToPreviousStep();
        }
        #endregion

        #region Stats
        else if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            Player.Instance.AddHp(-1);
            if (_isDebugVerbose) Debug.Log("[CHEAT] HP -1");
        }
        else if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            Player.Instance.AddHp(+1);
            if (_isDebugVerbose) Debug.Log("[CHEAT] HP +1");
        }
        else if (Input.GetKeyDown(KeyCode.G))
        {
            Cheat_AddGold();
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            Cheat_AddCombo();
        }
        #endregion

        #region GameFlow
        else if (Input.GetKeyDown(KeyCode.E))
        {
            LevelManager.Instance.CallEndGame();
            if (_isDebugVerbose) Debug.Log("[CHEAT] Call End Game");
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            LevelManager.Instance.ForceRestart();
            if (_isDebugVerbose) Debug.Log("[CHEAT] Restart Game");
        }
        #endregion

        #region SpawnStuffs
        else if (Input.GetKeyDown(KeyCode.P))
        {
            Cheat_SpawnBubble();
        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            Cheat_SpawnLoot();
        }
        else if (Input.GetKeyDown(KeyCode.M))
        {
            Cheat_SpawnMonster();
        }
        else if (Input.GetKeyDown(KeyCode.F))
        {
            Cheat_SpawnFeature();
        }
        #endregion

        #region Debug
        if (Input.GetKeyDown(KeyCode.X))
        {
            Cheat_ToggleGrid();
        }
        #endregion
    }
    */
}