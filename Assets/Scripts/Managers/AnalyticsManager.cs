﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using System.Linq;

public class AnalyticsManager : MonoBehaviour {
    /*
    bool _isDebugVerbose = false;

    public static AnalyticsManager Instance;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
        if (_isDebugVerbose) Debug.Log("[AnalyticsManager] Awake - " + Time.time);
    }

    void Start () {
        if (_isDebugVerbose) Debug.Log("[AnalyticsManager] Start - " + Time.time);		
	}
	
	public void LogNewProfile ()
    {
        if (_isDebugVerbose) Debug.Log("[AnalyticsManager] Log NewProfile - " + Time.time);
        Analytics.CustomEvent("profile.new", new Dictionary<string, object>
        {
            { "version", Profile.CURRENT_VERSION }
        });
    }

    public void LogLoadProfile()
    {
        if (_isDebugVerbose) Debug.Log("[AnalyticsManager] Log LoadProfile - " + Time.time);
        Analytics.CustomEvent("profile.load", new Dictionary<string, object>
        {
            { "version", Profile.CURRENT_VERSION },
            { "session", Profile.Instance.Data.SessionNbr },
            { "totalGold", Profile.Instance.Data.Inventory.Currency.Gold }
        });
    }

    public void LogEndGame(GameSetup gameSetup, bool isSkipEndGame)
    {
        if (_isDebugVerbose) Debug.Log("[AnalyticsManager] Log EndGame - Score : " + LevelTracker.Instance.Data.Score + " - " + Time.time);
        Analytics.CustomEvent("game.end", new Dictionary<string, object>
        {
            { "Name", gameSetup.GameMode == null ? "null" : gameSetup.GameMode.Name },
            { "score", LevelTracker.Instance.Data.Score },
            { "gold", LevelTracker.Instance.Data.InventoryLoot.Currency.Gold },
            { "maxSpeed", LevelTracker.Instance.Data.MaxSpeed },
            { "maxCombo", LevelTracker.Instance.Data.MaxCombo },
            { "height", LevelTracker.Instance.Data.TotalHeight },
            { "totalGold", Profile.Instance.Data.Inventory.Currency.Gold },
            { "isSkipEndGame", isSkipEndGame }
        });
    }

    public void LogMainQuest(QuestInstance questInstance)
    {
        int mainQuestIndex = GameDef.Instance.Data.MainQuests.FindIndex(x => x == questInstance.Data);
        if (mainQuestIndex < 0f)
            return;

        int totalUpgrades = Profile.Instance.Data.Upgrades.Sum(x => x.Level) - Profile.Instance.Data.Upgrades.Count;

        if (_isDebugVerbose) Debug.Log("[AnalyticsManager] Log MainQuest - Index : " + mainQuestIndex + " - Upgrades " + totalUpgrades + " - Gold/Spent : " + Profile.Instance.Data.Inventory.Currency.Gold + "/" + Profile.Instance.Data.GoldSpent + " - Launches : " + Profile.Instance.Data.ModePuzzle.Circles.Sum(x => x.NumberLaunches));
        Analytics.CustomEvent("quest.main", new Dictionary<string, object>
        {
            { "number", mainQuestIndex },
            { "totalUpgrades", totalUpgrades },
            { "totalGold", Profile.Instance.Data.Inventory.Currency.Gold },
            { "goldSpent", Profile.Instance.Data.GoldSpent },
            { "numberLaunches", Profile.Instance.Data.ModePuzzle.Circles.Sum(x => x.NumberLaunches) }
        });
    }
    */
}
