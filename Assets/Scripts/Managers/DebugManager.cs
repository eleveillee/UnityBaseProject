﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugManager : MonoBehaviour {

    public static DebugManager Instance;
    [SerializeField] GameObject _gridGo;

    private void Awake()
    {
        Instance = this;
        _gridGo.SetActive(false);
    }

    public void ToggleGrid() { _gridGo.SetActive(!_gridGo.activeInHierarchy); }

    public void DebugLog(string log)     { Debug.Log(log); }
    public void DebugWarning(string log) { Debug.LogWarning(log); }
    public void DebugError(string log)   { Debug.LogError(log); }
}
