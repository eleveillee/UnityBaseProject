﻿using UnityEngine;
using System.Collections;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public Action OnUpdate;

    public bool IsShowingSplashScreen = true;

    void Awake()
    {
        if (GameDef.Instance == null)
            UnityEngine.SceneManagement.SceneManager.LoadScene("scn_boot");

        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        LeanTween.init(600); // Hit the max number tween when it was default 400. Maybe I need to derefenrece tweens ??     
    }

    private void Start()
    {
        /*if (Profile.Instance.IsShowingDeletedDialog)
            UIManager.Instance.Dialog.Setup("Your save was deleted during the update. Sorry!", "SAVE DELETED", StartGame, null, null).Open();
        else
            StartGame();*/
    }

    void StartGame()
    {
        /*if (TutorialManager.Instance.IsEnabled && GameDef.Instance.Data.TutoCircle.StartingTutoInstance.IsEnabled && !Profile.Instance.Data.CompletedTutorials.Contains(Constants.STORY_TUTO1))// && Profile.Instance.Data.SessionNbr == 1
        {
            LevelManager.Instance.StartGameTuto();
        }
        else
        {
            LevelManager.Instance.FSM.ChangeState(LevelManager.Instance.FSM.Menu);
        }*/
    }

    void Update()
    {
        OnUpdate.SafeInvoke();
    }
}
