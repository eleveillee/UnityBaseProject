﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boot : BootStep {

    //SerializeField] List<BootStep> _steps;

    // Use this for initialization
    void Start ()
	{
        DontDestroyOnLoad(this);
		StartCoroutine(BootCR());
	}

	IEnumerator BootCR()
	{
		yield return new WaitForEndOfFrame();
		
		BootStep[] steps = GetComponentsInChildren<BootStep>();
		foreach (BootStep bootStep in steps)
		{
			if(bootStep.gameObject.activeInHierarchy)
				bootStep.Execute();

            yield return new WaitForEndOfFrame();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
