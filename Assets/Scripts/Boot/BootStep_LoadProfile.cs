﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootStep_LoadProfile : BootStep {

    [SerializeField] Profile _prefabProfile;
	public override void Execute()
	{
        Profile p = Instantiate(_prefabProfile);
        p.gameObject.name = "Profile";
        p.CreateOrLoadProfile();
	}
}
