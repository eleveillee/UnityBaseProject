﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootStep_InstantiateGenericGO : BootStep {
    
    [SerializeField] private GameObject _gameObject;
    [SerializeField] private string _name;

    public override void Execute()
    {
        GameObject go = Instantiate(_gameObject);
        go.name = _name;
        base.Execute();
    }
}
