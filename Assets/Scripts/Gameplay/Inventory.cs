﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Inventory
{
    public Currency Currency = new Currency();

    //Random/Add/Transfert/Remove
    #region Commented stuff. 
    /*internal static Inventory Random()
    {
        Inventory inventory = new Inventory();
        foreach (DataItem data in GameDef.Instance.Data.Items)
        {
            inventory.ItemList.Add(new ItemInstance(data, UnityEngine.Random.Range(0, 20)));
        }
        return inventory;
    }

    public void AddItem(DataItem dataItem, int value = 1)
    {
        ItemInstance itemInstance = ItemList.Find(x => x.Data == dataItem);
        itemInstance.Quantity = Mathf.Clamp(itemInstance.Quantity + value, 0, MaxSize);
        OnItemAdded.SafeInvoke();
    }

    internal void AddItem(ItemInstance itemInstance)
    {
        AddItem(itemInstance.Data, itemInstance.Quantity);
    }

    public bool IsFullOf(DataItem dataItem)
    {
        return ItemList.Find(x => x.Data == dataItem).Quantity >= MaxSize;
    }

    internal void AddInventory(Inventory loot)
    {
        foreach (ItemInstance itemInstance in loot.ItemList)
        {
            AddItem(itemInstance);
        }
    }

    internal bool Transfert(Inventory inventory, DataItem dataItem, int itemQuantity)
    {
        if (TryRemoveItem(dataItem, itemQuantity))
        {
            inventory.AddItem(dataItem, itemQuantity);
            return true;
        }

        return false;
    }

    public bool TryRemoveItem(DataItem dataItem, int value = 1)
    {
        ItemInstance itemInstance = ItemList.Find(x => x.Data == dataItem);
        if (itemInstance.Quantity < value)
        {
            return false;
        }

        itemInstance.Quantity = Mathf.Clamp(itemInstance.Quantity - value, 0, MaxSize);
        return true;
    }

    public bool TryRemoveInventory(Inventory inventory)
    {
        if (!HasInventory(inventory))
            return false;

        foreach(ItemInstance quantity in inventory.ItemList)
        {
            TryRemoveItem(quantity.Data, quantity.Quantity);
        }
        return true;
    }

    public bool HasInventory(Inventory inventory)
    {
        foreach (ItemInstance quantity in inventory.ItemList)
        {
            if(!HasItem(quantity.Data, quantity.Quantity))
            {
                return false;
            }
        }

        return true;
    }

    public bool HasItem(DataItem dataItem, int value = 1)
    {
        ItemInstance itemQuantity = ItemList.Find(x => x.Data == dataItem);
        if (itemQuantity == null)
        {
            //Debug.LogWarning("TRYING TO DELETE AN INEXISTENT ITEM  id :" + _itemId);
            return false;
        }

        return itemQuantity.Quantity >= value;
    }*/
    #endregion
}