using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Profile : MonoBehaviour
{
    public const float CURRENT_VERSION = 0.17f;
    public const float LASTSAFE_VERSION = 0.17f;
    public static Profile Instance;
    private bool _isDebugVerbose = false;
    private string _playerPrefName = "Save";
    public ProfileData Data = null;

    public float SaveCooldown = -1f;
    public bool IsSaveAllowed = false;
    //public static bool IsTutorial = true; // Hack To trigger tutorial on new profile
    public bool IsAlwaysNewProfile = true; // Hack to always create new profile
    public bool IsShowingDeletedDialog;

    //public Action<PlayerProfile> OnUpdate;
    public void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }

    public void CreateOrLoadProfile(bool isForceNew = false)
    {
        if (!isForceNew && !IsAlwaysNewProfile && PlayerPrefs.HasKey(_playerPrefName))
        {
            Data = Load();
            if(Data.Version < LASTSAFE_VERSION || Data.Version > CURRENT_VERSION)
            {
                if(_isDebugVerbose) Debug.LogWarning("[Profile] The save was deleted during initialization.");
                IsShowingDeletedDialog = true;
                DeleteProfile(true);
                return;
            }
            else
            {
                InitializeLoadedProfile();
            }
        }
        else
        {
            CreateNewProfile();
        }

        Data.SessionNbr++;
        if(SaveCooldown > 0f)
            StartCoroutine(Save_CR());
    }

    void CreateNewProfile()
    {
        Data = new ProfileData();
        Data.InitializeNewProfile();
        //AnalyticsManager.Instance.LogNewProfile();
        if (_isDebugVerbose) Debug.Log("[Profile] New Profile");
    }

    void InitializeLoadedProfile()
    {
        Data.FillAllList();
        //AnalyticsManager.Instance.LogLoadProfile();
        if (_isDebugVerbose) Debug.Log("[Profile] LoadProfile Version" + Data.Version);
    }

    private IEnumerator Save_CR()
    {
        Save();
        yield return new WaitForSeconds(SaveCooldown);
        StartCoroutine(Save_CR());
    }

    #region Save
    public void Save()
    {
        if (!IsSaveAllowed)
            return;

        //Debug.Log("Save: " + DateTime.Now);
        Data.TimeSave = DateTime.Now.Ticks;

        string saveJson = Serialize();
        PlayerPrefs.SetString(_playerPrefName, saveJson);
        if (_isDebugVerbose) Debug.Log("[SaveSystem] Save to Json : " + saveJson);
    }

    public string Serialize()
    {
        return JsonUtility.ToJson(Data);
    }
    #endregion

    #region Load
    public ProfileData Load()
    {
        string jsonSave = PlayerPrefs.GetString(_playerPrefName);
        if (_isDebugVerbose) Debug.Log("[SaveSystem] Load from Json : " + jsonSave);
        return Deserialize(jsonSave);
    }

    public ProfileData Deserialize(string jsonSave)
    {
        return JsonUtility.FromJson<ProfileData>(jsonSave);
    }
    #endregion

    public void DeleteProfile(bool isCreatingNew = false)
    {
        if (_isDebugVerbose) Debug.Log("[SaveSystem] Deleted Profile");
        PlayerPrefs.DeleteAll();
        if (isCreatingNew)
            CreateOrLoadProfile(true);
    }

    public void CleanGenericList<T>(List<T> list, string origin)
    {
        List<T> nullTs = list.FindAll(x => x == null);
        if (nullTs == null || nullTs.Count <= 0)
            return;

        Debug.LogError("[Profile] Profile returned " + list.Count + " Nulls in " + origin);
        UIManager.Instance.Dialog.Setup("Some save datas might have been lost from last version. Code:" + origin, "Partial save lost");

        list.RemoveAll(x => x == null);
    }

    /*
    public static void AllowSave(bool isAllowed)
    {
        _isSaveAllowed = isAllowed;
    }*/
}