﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class ProfileData
{
    
    // Profile
    [SerializeField] private float _version;
    public float Version { get { return _version; } }
    public long TimeSave;
    public int SessionNbr;
    public int GoldSpent;
        
    public bool IsSkippingTutorials;
    public bool HasBeatenFirstBoss; //ToDo replace by DataCondition
    
    // Settings
    public bool IsMutedMusic;
    public bool IsMutedSFX;

    // Tutorials
    public List<String> CompletedTutorials = new List<string>();

    // Quests
    //public QuestInstance MainQuest;
    //public List<QuestInstance> Quests = new List<QuestInstance>();

    // Trackings
    public TrackingData CumulativeTrackingData;
    public TrackingData PersistentTrackingData;

    public Bestiary Bestiary = new Bestiary();
    public Inventory Inventory = new Inventory();

    public void InitializeNewProfile() //Todo: Use a BaseProfileData from GameDef like BaseCity
    {
        _version = Profile.CURRENT_VERSION;
        FillAllList();
        

        // Quests
        //QuestManager.Instance.StartNextMainQuest();
    }

    public void FillAllList()
    {
    }

    public void AddGold(int value)
    {
        Inventory.Currency.Add(value);
    }
    
}

/*
[Serializable]
public class UpgradeInstance
{
    public DataUpgrade Data;
    public int Level = 1;

    //public float Value { get {Debug.Log("Level : " + Level); Debug.Log("Data : " + Data.Name); Debug.Log("TableCount : " + Data.Table.Count);  return Data.Table[Level - 1].Value; } }
    public float Value { get { return (Data == null ? -1f : Data.Table[Level - 1].Value); } }

    public int Price { get { return (Data == null ? -1 : Data.Table[Level].Price); } }
    public bool IsBuyable { get { return Profile.Instance.Data.Inventory.Currency.Gold >= Price; } }
    public UpgradeInstance Setup(DataUpgrade data)
    {
        //if (GameDef.Instance == null)
        //    return;

        Data = data;
        return this;
    }

    public void TryUpgrade()
    {
        if (Profile.Instance.Data.Inventory.Currency.TrySpend(Price))
        {
            Level++;
            Profile.Instance.Save();
        }
            
    }
}
*/
