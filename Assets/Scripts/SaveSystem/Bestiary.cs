﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Bestiary
{
    public List<MonsterInstance> Monsters = new List<MonsterInstance>();

    public void FillList()
    {
        /*foreach (DataMonster dataMonster in GameDef.Instance.Data.Monsters)
        {
            if (Monsters.Find(x => x.Data == dataMonster) != null)
                continue;

            MonsterInstance mInstance = new MonsterInstance(dataMonster);
            Monsters.Add(mInstance);
        }*/

        Profile.Instance.CleanGenericList(Monsters, "Bestiary");
    }

    public void Add(Bestiary bestiary)
    {
        foreach(MonsterInstance mi in bestiary.Monsters)
        {
            Add(mi.Data, mi.Value);
        }
    }

    // Tracking data bestiary are not filled to reduce space size
    public void Add(DataMonster dataMonster, int value)
    {
        MonsterInstance mi = Monsters.Find(x => x.Data == dataMonster);
        if(mi == null)
        {
            Monsters.Add(new MonsterInstance(dataMonster, value));
        }
        else
        {
            mi.Value += value;
        }
    }
}

[Serializable]
public class MonsterInstance
{
    public DataMonster Data;
    public int Value;

    public MonsterInstance(DataMonster data, int value = 0)
    {
        Data = data;
        Value = value;
    }
}