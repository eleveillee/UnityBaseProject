﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FSM
{
    public abstract class FSM : MonoBehaviour
    {
        bool _isDebugVerbose = false;
        List<State> _states = new List<State>();

        public State CurrentState { get; private set; }
        /*public virtual void DefineStates() { }

        public void StartMachine()
        {
            DefineStates();
            //StartCoroutine(ExecuteQueue());
        }*/

        public virtual IEnumerator ExecuteQueue()
        {
            foreach(State state in _states)
            {
                ChangeState(state);
                /*if(state.IsInstant)
                {
                    state.Exit();
                }
                else
                {
                    //yield return new wait;
                }*/
            }

            yield return null;
        }

        void Update()
        {
            if (CurrentState != null && CurrentState.IsInitialized)
                CurrentState.Update();
        }

        protected void AddState(State state)
        {
            _states.Add(state);
            state.Initialize(this);
        }

        public void ChangeState(State state)
        {
            if(CurrentState != null)
                CurrentState.Exit();

            if(_isDebugVerbose)
                Debug.Log("[FSM] ChangeState : " + (CurrentState != null ? CurrentState + " -> " : "") + state);

            CurrentState = state;
            CurrentState.Initialize(this);
            CurrentState.Enter();
        }
    }
}


