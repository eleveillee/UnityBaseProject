﻿using System;
using UnityEngine;
using System.Collections;

namespace FSM
{
    // Childrens need [Serializable]
    public abstract class State
    {
        protected FSM _fsmRef;
        public virtual bool IsInstant { get { return true; } }

       [NonSerialized] public bool IsInitialized;

        public void Initialize(FSM fsm)
        {
            _fsmRef = fsm;
        }

        public void Enter() { OnEnter(); IsInitialized = true; }
        protected virtual void OnEnter() { }

        public void Update() { OnUpdate(); }
        protected virtual void OnUpdate() { }
        
        public void Exit() { OnExit(); }
        protected virtual void OnExit() { }
    }
}


