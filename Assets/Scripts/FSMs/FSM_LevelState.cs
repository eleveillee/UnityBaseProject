﻿using System;
using UnityEngine;
using System.Collections;

public class FSM_LevelState : FSM.FSM
{
    public LevelState_Menu Menu;
    public LevelState_PreGame PreGame;
    public LevelState_Launcher Launcher;
    public LevelState_Playing Playing;
    public LevelState_EndGame EndGame;
}


