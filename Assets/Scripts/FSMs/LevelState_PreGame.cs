﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class LevelState_PreGame : FSM.State
{
    /*
    bool _isDebugVerbose = false;

    public bool IsSkippingLauncher;

    GameSetup _newGameSetup;
    protected override void OnEnter()
    {
        if (_isDebugVerbose) Debug.Log("[PreGame] OnEnter");
        Profile.Instance.Save();

        // UI
        UIManager.Instance.CloseAllPanels();
        UIManager.Instance.Overlay.Show(true);

        // World
        WorldManager.Instance.CleanUp();
        PauseManager.Instance.Pause(false);

        
        ActuallyStartGame();
        //if (Profile.Instance.Data.HasBeatenFirstBoss || Profile.Instance.Data.IsSkippingTutorials)
        //    ActuallyStartGame();
        //else
        //    UIManager.Instance.PanelHelper.Setup(ActuallyStartGame).Open();
    }

    void ActuallyStartGame()
    {
        if (_isDebugVerbose) Debug.Log("[PreGame] ActuallyStartGame");
        bool isTutoInstanceSkipLauncher = TutorialManager.Instance.CurrentTutoInstance != null && TutorialManager.Instance.CurrentTutoInstance.IsSkippingLauncher;
        IsSkippingLauncher = (CheatManager.Instance.IsSkipLauncher || _newGameSetup.GameMode.IsSkipLauncher || isTutoInstanceSkipLauncher);
        //Debug.Log("IsSkippingLauncher[" + IsSkippingLauncher + "] =  CM :" + CheatManager.Instance.IsSkipLauncher + " - GM : " + _newGameSetup.GameMode.IsSkipLauncher + " - Tuto : " + isTutoInstanceSkipLauncher);
        WorldManager.Instance.SpawnWorld(_newGameSetup);

        //Tuto
        TutorialInstance tutoInstance = _newGameSetup.StartingCircle.StartingTutoInstance;

        if (tutoInstance != null && tutoInstance.IsEnabled && TutorialManager.Instance.IsEnabled)
            TutorialManager.Instance.Activate(_newGameSetup.StartingCircle.StartingTutoInstance);
        
        if (IsSkippingLauncher)
            SkipLauncher();
        else
            StartLauncher();

        LevelTracker.Instance.Setup(_newGameSetup);
    }

    void StartLauncher()
    {
        if (_isDebugVerbose) Debug.Log("[PreGame] StartLauncher");
        LevelManager.Instance.FSM.ChangeState(LevelManager.Instance.FSM.Launcher);
    }

    void SkipLauncher()
    {
        if (_isDebugVerbose) Debug.Log("[PreGame] SkipLauncher");
        LevelManager.Instance.FSM.ChangeState(LevelManager.Instance.FSM.Playing);
    }

    protected override void OnExit()
    {
    }

    public void Setup(GameSetup newGameSetup)
    {
        _newGameSetup = newGameSetup;
    }
    */
}


