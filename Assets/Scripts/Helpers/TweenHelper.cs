﻿using System;
using UnityEngine;

using Random = UnityEngine.Random;

public static class TweenHelper {
    
    public static LTDescr BumpIn(GameObject go, float duration, float scaleMax, bool isStartingFromZero, Action onComplete = null)
    {
        //Initialize
        Vector3 initialSize = go.transform.localScale;
        if(isStartingFromZero)
            go.transform.localScale = Vector3.zero;

        //Tween
        LTDescr lt =             LeanTween.scale(go, initialSize * 1.25f, duration*0.8f).setEaseOutBack();
        lt.setOnComplete(() => { LeanTween.scale(go, initialSize, duration * 0.2f).setEaseInBack().setOnComplete(() => {
                if (onComplete != null) onComplete.SafeInvoke(); });
        });

        return lt;
    }
}