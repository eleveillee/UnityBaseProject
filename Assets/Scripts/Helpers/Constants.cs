﻿using System;
using System.Reflection;
using UnityEngine;
using Random = UnityEngine.Random;

public static class Constants {

    #region MonsterProperties

    // Generics
    public static string TRUE = "True";
    public static string FALSE = "False";

    // Fodder
    public static string MP_IS_BIG = "IsBig";

    // Shield
    public static string SHIELD_ID = "ShieldId";
    public static string SHIELDED_BY_ID = "ShieldedById";
    #endregion

    #region Layers
    public const int LAYER_PLAYER = 9;
    public const int LAYER_PLAYER_NOMONSTER = 10;

    public const int LAYER_MONSTER = 11;
    public const int LAYER_MONSTER_NOWALL = 12;
    #endregion

    #region UI Sprite
    public static string UI_SPRITE_HP { get { return GetSprite("Hp"); } } //public static string UI_SPRITE_HP { get { return GetSprite(0); } }
    public static string UI_SPRITE_GOLD { get { return GetSprite("Gold"); } } //public static string UI_SPRITE_GOLD { get { return GetSprite(1); } }
    public static string UI_SPRITE_ENERGY { get { return GetSprite("Energy"); } } //public static string UI_SPRITE_ENERGY { get { return GetSprite(2); } }
    public static string UI_SPRITE_COMBO { get { return GetSprite("Combo"); } } //public static string UI_SPRITE_COMBO { get { return GetSprite(3); } }

    public static string STORY_TUTO1 = "1_GateOfHell";

    static string GetSprite(int index)
    {
        return " <sprite index=" + index + ">";
    }

    static string GetSprite(string name)
    {
        return " <sprite name=\"" + name + "\">";
    }
    #endregion

    #region Animations

    public static string ANIM_TRIGGER_ATTACK = "Attack";
    public static string ANIM_TRIGGER_IDLE = "Idle";
    public static string ANIM_TRIGGER_MOVE = "Move";
    public static string ANIM_TRIGGER_TELEGRAPH = "Telegraph";
    public static string ANIM_TRIGGER_DROP = "Drop";

    #endregion
}
