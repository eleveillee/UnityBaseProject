﻿using System;
using UnityEngine;

public static class Helper
{
    //Random
    public static float RandomSign { get { return Random ? -1f : 1f; } }
    public static bool Random { get { return UnityEngine.Random.Range(0, 1f) <= 0.5f; } }

    //GetSign
    public static float GetSign(bool isPositive) { return isPositive ? 1f : -1f; }
        
    //Gizmos
    public static void DrawGizmosArea(Vector2 vx, Vector2 vy, Color color)
    {
        Gizmos.color = color;
        Vector3 pupCenter = new Vector3(vx.Sum(), vy.Sum(), 0f) / 2f;
        Vector3 pupSize = new Vector3(vx.Sub(), vy.Sub(), 0f);
        Gizmos.DrawWireCube(pupCenter, pupSize);
    }
}

public enum Direction
{
    Left,
    Up,
    Right,
    Down
}
