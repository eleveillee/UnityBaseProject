using UnityEngine;
using System.Collections.Generic;
using System;

// Manage stories and iterate trought TutoInstances
public class TutorialManager : MonoBehaviour
{
    private bool _isDebugVerbose = false;
    public static TutorialManager Instance;

    TutorialInstance _currentTutoInstance;
    public TutorialInstance CurrentTutoInstance { get { return _currentTutoInstance; }}

    public bool IsEnabled = true;

    void Awake()
    {
        Instance = this;
    }
    
    public void Activate(TutorialInstance tutoInstancePrefab)
    {
        if (!tutoInstancePrefab.IsEnabled || !IsEnabled)
        {
            Debug.LogWarning("[TutorialManager] Trying to activate a disabled tutorial : " + tutoInstancePrefab.name);
            return;
        }

        _currentTutoInstance = (TutorialInstance)Instantiate(tutoInstancePrefab, transform);
        if(_isDebugVerbose) Debug.Log("[TutorialManager] Activated new TutoInstance : " + _currentTutoInstance.gameObject.name);
        _currentTutoInstance.gameObject.SetActive(true);
        _currentTutoInstance.Activate();
    }
    
    public void CompleteCurrentTutoInstance()
    {
        if (_currentTutoInstance == null)
            return;

        if (_isDebugVerbose) Debug.Log("[TutorialManager] Complete Tutorial : " + _currentTutoInstance.Name + " (" + _currentTutoInstance.Id + ")");
        Profile.Instance.Data.CompletedTutorials.Add(_currentTutoInstance.Id);
        _currentTutoInstance = null;
    }
    
    /*internal void StopCurrentTuto()
    {
        if (_currentTutoInstance == null)
            return;

        Destroy(_currentTutoInstance.gameObject);
        _currentTutoInstance = null;
    }*/

    // Debugs
    public void Debug_SkipAllTutorials()
    {
        if (_isDebugVerbose) Debug.Log("[TutorialManager_Dbg] SkipAllTutorials");
        Profile.Instance.Data.IsSkippingTutorials = true;
        CompleteCurrentTutoInstance();

        //CleanUp
        //Debug_FinishTutoEncounter();
        UIManager.Instance.Dialog.Close();
    }

    public void Debug_CompleteTutorial()
    {
        if (_isDebugVerbose) Debug.Log("[TutorialManager_Dbg] Complete Tutorial : " + _currentTutoInstance.Name);
        CompleteCurrentTutoInstance();
        
        //CleanUp
        //Debug_FinishTutoEncounter();
        UIManager.Instance.Dialog.Close();
    }

    public void Debug_CompleteStep()
    {
        if (_currentTutoInstance == null)
            return;

        if (_isDebugVerbose) Debug.Log("[TutorialManager_Dbg] Complete Step : " + _currentTutoInstance.Name + " (" + _currentTutoInstance.CurrentStep + ")");
        _currentTutoInstance.Debug_CompleteStep();
    }

    public void Debug_ReturnToPreviousStep()
    {
        if (_isDebugVerbose) Debug.Log("[TutorialManager_Dbg] Complete Step : " + _currentTutoInstance.Name + " (" + _currentTutoInstance.CurrentStep + ")");
        _currentTutoInstance.Debug_ActivatePreviousStep();
    }
}