using UnityEngine;
using System.Collections.Generic;
using System;

public class TutoStepHideScreen : TutoStep
{
    [SerializeField] [Tooltip("seconds. 0 is instant")]
    public float _fadeTime;
    
    [SerializeField] public float _opacity = 0.5f;
    [SerializeField] private bool _isHidingAtEndOfStep;
    public override void Activate()
    {
        UIManager.Instance.HideScreen.ChangeOpacity(_opacity);

        if (_fadeTime > 0f)
            LeanTween.delayedCall(gameObject, _fadeTime, Hide);
    }

    void Hide()
    {
        UIManager.Instance.HideScreen.Show(false);
    }

    public override void OnDeactivate()
    {
        if(_isHidingAtEndOfStep)
            Hide();
    }
}