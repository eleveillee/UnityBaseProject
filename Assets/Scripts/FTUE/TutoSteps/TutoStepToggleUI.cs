using UnityEngine;
using System.Collections.Generic;
using System;

public class TutoStepToggleUI : TutoStep
{
    bool _isDebugVerbose = false;
    [SerializeField] private bool _isUIVisible;
    [SerializeField] private bool _isAutoReactivatedAtEnd;
    [SerializeField] private float _duration;

    public override void Activate()
    {
        if (_duration < 0f || UIManager.Instance == null)
            return;

        //if (_isDebugVerbose) Debug.Log("[TutoStepToggleUI] Disable Controls (" + _duration + " sec)");
        ToggleUI(_isUIVisible);

        if (_duration > 0f)
            LeanTween.delayedCall(gameObject, _duration, () => ToggleUI(_isUIVisible));
    }

    public override void OnDeactivate()
    {
        if(_isAutoReactivatedAtEnd)
            ToggleUI(true);
    }

    void ToggleUI(bool isVisible)
    {
        if (UIManager.Instance == null || UIManager.Instance.Overlay == null)
            return;

        if (_isDebugVerbose) Debug.Log("[TutoStepBlockInputs] Toggle UI : " + _isUIVisible);

        UIManager.Instance.Overlay.Show(_isUIVisible);
    }
}