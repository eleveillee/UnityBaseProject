using UnityEngine;
using System.Collections.Generic;
using System;

public class TutoStepToggleInputs : TutoStep
{
    bool _isDebugVerbose = false;
    [SerializeField] private bool _isInputsActivated;
    [SerializeField] private bool _isAutoReactivatedAtEnd;
    [SerializeField] private float _duration;

    public override void Activate()
    {
        /*if (_duration < 0f || Player.Instance == null || Player.Instance.Controller == null)
            return;

        if (_isDebugVerbose) Debug.Log("[TutoStepBlockInputs] Disable Controls (" + _duration + " sec)");
        Player.Instance.Controller.IsEnabled = _isInputsActivated;

        if (_duration > 0f)
            LeanTween.delayedCall(gameObject, _duration, ReactivateControls);*/
    }

    public override void OnDeactivate()
    {
        if(_isAutoReactivatedAtEnd)
            ReactivateControls();
    }

    void ReactivateControls()
    {
        /*if (Player.Instance == null || Player.Instance.Controller == null)
            return;

        if (_isDebugVerbose) Debug.Log("[TutoStepBlockInputs] Reactivate Controls");

        Player.Instance.Controller.IsEnabled = true;*/
    }
}