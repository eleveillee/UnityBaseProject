using UnityEngine;
using System.Collections;
using System.Text;

public class TutoStepDialog : TutoStep
{
    public string Text = "Missing Text";
    private UIDialog _uiDialog;
    [SerializeField] private bool _isClosingAfterStep;

    //private StringBuilder _liveText;
    //int _currentIndex = -1;

    public override void Activate()
    {
        //_liveText = new StringBuilder();
        _uiDialog = UIManager.Instance.Dialog.Setup(Text, "Tuto");
        _uiDialog.Open();
        //StartCoroutine(WriteText());
    }

    public override void OnDeactivate()
    {
        if (_isClosingAfterStep)
            _uiDialog.Close();
    }

    //Write Text RunTime
    /* IEnumerator WriteText()
    {
    //Todo Kill Coroutine
        while(_currentIndex < Text.Length - 1)
        {
            _liveText.Append(Text[++_currentIndex]);
            _uiDialog.UpdateText(_liveText);
            yield return new WaitForEndOfFrame();
            //yield return new WaitForSeconds(0.005f);
        }
    }*/
}
