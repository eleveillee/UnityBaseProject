using UnityEngine;
using System.Collections.Generic;
using System;

public class TutoStepSlowDown : TutoStep
{
    bool _isDebugVerbose = false;
    [SerializeField] private float _ratio;
    [Tooltip("Time to slowdown the player")]
    [SerializeField] private float _timeToSlowDown;
    [SerializeField] private bool _isAutoReactivatedAtEnd;
    //[SerializeField] private float _duration;

    public override void Activate()
    {
        SlowDown();
        /*if (_duration < 0f || UIManager.Instance == null)
            return;

        //if (_isDebugVerbose) Debug.Log("[TutoStepToggleUI] Disable Controls (" + _duration + " sec)");
        SlowDown(_isUIVisible);

        if (_duration > 0f)
            LeanTween.delayedCall(gameObject, _duration, () => ToggleUI(_isUIVisible));*/
    }

    public override void OnDeactivate()
    {
        /*if(_isAutoReactivatedAtEnd)
            ToggleUI(true);*/
    }

    void SlowDown()
    {
        //LeanTween.value(Player.Instance.gameObject, (x) => { Player.Instance.SetSpeed(x); }, _launchSpeedPeak, _launchSpeedEnd, _launchTime*launchRatio).setEase(LeanTweenType.easeInCubic);
        float initialSpeed = Time.timeScale;
        if (_timeToSlowDown > 0f)
            LeanTween.value(gameObject, (x) => Time.timeScale = x, initialSpeed, _ratio, _timeToSlowDown).setIgnoreTimeScale(true);
        else
            Time.timeScale = _ratio;

        if (_isDebugVerbose) Debug.Log("[TutoStepSlowDown] SlowDown : " + _ratio);
    }
}