using System;
using System.Linq;
using System.Collections;
using UnityEngine;
using Lean;

[Serializable]
public class TutoConditionTap : TutoCondition
{
    public override void OnInitialize()
    {
        LeanTouch.OnFingerDown += OnFingerDown;
        GameManager.Instance.OnUpdate += OnUpdate;
    }

    void OnFingerDown(LeanFinger fingers)
    {
        _isComplete = true;
    }
    
    override protected void OnUpdate()
    {
        if (_isComplete)
            GameManager.Instance.StartCoroutine(ReverseCompleteNextFrame());
    }

    IEnumerator ReverseCompleteNextFrame()
    {
        yield return null;
        _isComplete = false;
    }

    public override void OnDisable()
    {
        LeanTouch.OnFingerDown -= OnFingerDown;
        GameManager.Instance.OnUpdate -= OnUpdate;
    }
}
