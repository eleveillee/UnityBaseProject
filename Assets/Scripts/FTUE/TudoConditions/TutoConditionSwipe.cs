using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;

[Serializable]
public class TutoConditionSwipe : TutoCondition
{
    private bool _isDebugVerbose = false;
    private Direction _direction;
    float _deltaThreshold = 50f;

    public override void OnInitialize()
    {
        if (Values.IsNullOrEmpty())
        {
            Debug.LogWarning("[TutoConditionSwipe] Missing value");
            return;
        }
            

        LeanTouch.OnFingerSwipe += OnFingerSwipe;
        _direction = (Direction)int.Parse(Values[0]);

        if (_isDebugVerbose) Debug.Log("[TutoConditionSwipe] Direction : " + _direction);
    }

    void OnFingerSwipe(LeanFinger fingers)
    {
        if (Values.IsNullOrEmpty())
            return;

        _isComplete = false;
        if (Mathf.Abs(fingers.TotalDeltaScreenPosition.x) > _deltaThreshold)
            _isComplete = (fingers.TotalDeltaScreenPosition.x > 0 && _direction == Direction.Right) || (fingers.TotalDeltaScreenPosition.x < 0 && _direction == Direction.Left);
        else if (fingers.TotalDeltaScreenPosition.y > _deltaThreshold)
            _isComplete = (fingers.TotalDeltaScreenPosition.y > 0);

        if(_isDebugVerbose) Debug.Log("[TutoConditionSwipe] OnFingerSwipe : " + _isComplete);
    }
    
    protected override void OnUpdate()
    {
        if (Values.IsNullOrEmpty())
            return;

        if (Input.GetKeyDown(KeyCode.A) && _direction == Direction.Left  ||
            Input.GetKeyDown(KeyCode.W) && _direction == Direction.Up    ||
            Input.GetKeyDown(KeyCode.D) && _direction == Direction.Right ||
            Input.GetKeyDown(KeyCode.S) && _direction == Direction.Down)
            _isComplete = true;
    }

    /*void OnUpdate()
    {
        if (_isComplete)
            GameManager.Instance.StartCoroutine(ReverseCompleteNextFrame());
    }*/

    IEnumerator ReverseCompleteNextFrame()
    {
        yield return null;
        _isComplete = false;
    }

    public override void OnDisable()
    {
        LeanTouch.OnFingerSwipe -= OnFingerSwipe;
        //GameManager.Instance.OnUpdate -= OnUpdate;
    }
}
