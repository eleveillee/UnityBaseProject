using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Lean;
public class TutoCompleteCondition : MonoBehaviour
{
    [SerializeField]
    EnumOperator _operator;

    public float Delay;
    private int _currentStep;
    private bool _isCompleted;

    [SerializeField]
    List<TutoConditionDefinition> _conditions = new List<TutoConditionDefinition>();

    [SerializeField]
    List<TutoCondition> _TutoConditions = new List<TutoCondition>();

    public Action OnComplete;
    
    public void Initialize()
    {
        _TutoConditions.Clear();
        foreach (TutoConditionDefinition condition in _conditions)
        {
            _TutoConditions.Add(GetCondition(condition));
        }
    }

    void Update()
    {
        if (_TutoConditions.IsNullOrEmpty())
            return;

        _TutoConditions.ForEach(x => x.Update());

        //Debug.Log("(" + TutorialManager.Instance.CurrentTutoInstance.CurrentStep + ") AreConditionCompleted : " + AreConditionsCompleted());
        if (AreConditionsCompleted() && !_isCompleted)
        {
            if (Delay > 0)
                LeanTween.delayedCall(Delay, Complete);
            else
                Complete();
        }
    }

    void Complete()
    {
        _isCompleted = true;
        OnComplete.SafeInvoke();
    }

    TutoCondition GetCondition(TutoConditionDefinition condition)
    {
        TutoCondition TutoCondition;
        switch (condition.Type)
        {
            #region ConditionCases
            case EnumTutoConditionType.Hold:
                TutoCondition = new TutoConditionHold();
                break;
            case EnumTutoConditionType.Tap:
                TutoCondition = new TutoConditionTap();
                break;
            case EnumTutoConditionType.Time:
                TutoCondition = new TutoConditionTime();
                break;
            case EnumTutoConditionType.True:
                TutoCondition = new TutoConditionTrue();
                break;
            case EnumTutoConditionType.False:
                TutoCondition = new TutoConditionFalse();
                break;
            case EnumTutoConditionType.Swipe:
                TutoCondition = new TutoConditionSwipe();
                break;
            case EnumTutoConditionType.MonsterKilled:
                TutoCondition = new TutoConditionMonsterKilled();
                break;
            default:
                Debug.LogWarning("[StorySteTutoCondition] Wrong condition type : " + condition.Type);
                return null;
                #endregion
        }

        TutoCondition.Initialize(condition.Values);
        return TutoCondition;
    }

    bool AreConditionsCompleted()
    {
        bool isCompleted = _operator == EnumOperator.AND;
        foreach (TutoCondition condition in _TutoConditions)
        {
            if(_operator == EnumOperator.AND)
                isCompleted &= condition.IsComplete;
            else
                isCompleted |= condition.IsComplete;
        }

        return isCompleted;
    }
    
    public List<TutoConditionDefinition> Conditions
    {
        get { return _conditions; }
    }

    public List<TutoCondition> TutoConditions
    {
        get { return _TutoConditions; }
    }


    public EnumOperator Operator
    {
        get { return _operator; }
    }
}

[Serializable]
public class TutoConditionDefinition
{
    public EnumTutoConditionType Type;
    public List<string> Values = new List<string>();
}

public enum EnumTutoConditionType
{
    Custom,
    Time,
    Tap,
    Hold,
    EncounterStart,
    True,
    False,
    Swipe,
    MonsterKilled
}