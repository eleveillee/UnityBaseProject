using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Lean;

[Serializable]
public class TutoConditionHold : TutoCondition
{
    float _timeStart;


    public override void OnInitialize()
    {
        LeanTouch.OnFingerHeldDown += OnFingerHeldDown;
        LeanTouch.OnFingerHeldUp += OnFingerHeldUp;
    }

    public override void OnDisable()
    {
        LeanTouch.OnFingerHeldDown -= OnFingerHeldDown;
        LeanTouch.OnFingerHeldUp -= OnFingerHeldUp;
    }

    void OnFingerHeldDown(LeanFinger fingers)
    {
        _isComplete = true;
    }

    void OnFingerHeldUp(LeanFinger fingers)
    {
        _isComplete = false;
    }
}
