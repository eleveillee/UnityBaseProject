using System;
using UnityEngine;

[Serializable]
public class TutoConditionMonsterKilled : TutoCondition
{
    /*DataMonster _dataMonster;
    int _numberKills;
    bool _isInitialized;

    public override void OnInitialize()
    {
        if(Values.IsNullOrEmpty())
        {
            Debug.LogWarning("[TutoConditionMonsterKilled] Empty Values");
            return;
        }

        // DataMonster
        if (Values.Count < 1 || String.IsNullOrEmpty(Values[0]))
        {
            Debug.LogWarning("[TutoConditionMonsterKilled] Value missing : [DataMonster]");
            return;
        }

        string monsterName = Values[0];
        _dataMonster = GameDef.Instance.Data.Monsters.Find(x => x.Name == monsterName);

        if(_dataMonster == null)
        {
            Debug.LogWarning("[TutoConditionMonsterKilled] Incorrect value : [DataMonster]");
            return;
        }
        
        // NumberKills
        if (Values.Count < 2 || !int.TryParse(Values[1],out _numberKills) || _numberKills <= 0)
        {
            Debug.LogWarning("[TutoConditionMonsterKilled] Incorrect value : [NumberKills]");
            return;
        }

        _isInitialized = true; 
    }

    override protected void OnUpdate()
    {
        if (!_isInitialized)
            return;

        MonsterInstance monsterInstance = LevelTracker.Instance.Data.Bestiary.Monsters.Find(x => x.Data == _dataMonster);
        if (monsterInstance != null && monsterInstance.Value >= _numberKills)
        {
            _isInitialized = false;
            _isComplete = true;
        }   
    }
    */
}
