using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Lean;

// Base condition to complete a tuto step
[Serializable]
public abstract class TutoCondition
{
    public EnumTutoConditionType Type;
    public List<string> Values = new List<string>();

    public void Initialize(List<string> values)
    {
        Values = values;
        OnInitialize();
    }

    ~TutoCondition()
    {
        OnDisable();
    }

    public virtual void OnInitialize(){}
    public virtual void OnDisable(){}

    public void Update() { OnUpdate(); }
    protected virtual void OnUpdate() { }

    protected bool _isComplete;
    public bool IsComplete
    {
        get { return (_isComplete || Type == EnumTutoConditionType.True) && Type != EnumTutoConditionType.False; }
        set { _isComplete = value;  }
    }
}
