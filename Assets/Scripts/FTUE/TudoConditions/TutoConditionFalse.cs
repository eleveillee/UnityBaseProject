using System;

[Serializable]
public class TutoConditionFalse : TutoCondition
{
    float _timeStart;

    public override void OnInitialize()
    {
        _isComplete = false;
    }
}
