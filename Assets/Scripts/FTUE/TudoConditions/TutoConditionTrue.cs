using System;

[Serializable]
public class TutoConditionTrue : TutoCondition
{
    float _timeStart;

    public override void OnInitialize()
    {
        _isComplete = true;
    }
}
