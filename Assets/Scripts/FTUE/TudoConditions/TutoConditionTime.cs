using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Lean;

[Serializable]
public class TutoConditionTime : TutoCondition
{
    float _timeStart;

    public override void OnInitialize()
    {
        _timeStart = Time.unscaledTime;
        GameManager.Instance.OnUpdate += OnUpdate;
    }

    public override void OnDisable()
    {
        GameManager.Instance.OnUpdate -= OnUpdate;
    }

    override protected void OnUpdate()
    {
        if (Values.IsNullOrEmpty())
        {
            Debug.LogWarning("[TutoConditionTime] Missing time value");
            return;
        }

        if (Time.unscaledTime - _timeStart > float.Parse(Values[0])) // Test for duration
            _isComplete = true;
    }
}
