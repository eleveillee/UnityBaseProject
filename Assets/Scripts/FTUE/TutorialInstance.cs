using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

// A instance of a tuto. Consist of many steps
public class TutorialInstance : MonoBehaviour
{
    private bool _isDebugVerbose = false;
    public string Id;
    public string Name;
    public bool IsEnabled;
    public bool IsSkippingLauncher;

    [NonSerialized]
    public List<TutoCompleteCondition> StepHolders = new List<TutoCompleteCondition>();

    [NonSerialized]
    public int CurrentStep;
    bool _isCompleted = false;

    void Awake()
    {
        StepHolders = GetComponentsInChildren<TutoCompleteCondition>().ToList();
        StepHolders.RemoveAll(x => !x.gameObject.activeInHierarchy);
        StepHolders.ForEach(x => x.gameObject.SetActive(false));
    }

    public void Activate()
    {
        if (!IsEnabled)
            Debug.LogWarning("[TutorialInstance] Trying to activate a disabled TutoInstance");

        ActivateStep(0);
    }

    void ActivateStep(int stepIndex)
    {
        if (_isCompleted)
            return;

        CurrentStep = stepIndex;
        StepHolders[CurrentStep].gameObject.SetActive(true);
        TutoCompleteCondition tutoCompleteCondition = StepHolders[CurrentStep].GetComponent<TutoCompleteCondition>();
        tutoCompleteCondition.OnComplete += OnCurrentStepCompleted;
        tutoCompleteCondition.Initialize();

        foreach (TutoStep tutoStep in StepHolders[CurrentStep].GetComponents<TutoStep>())
        {
            tutoStep.Activate();
        }
    }
    
    void OnCurrentStepCompleted()
    {
        if (_isCompleted || StepHolders[CurrentStep] == null)
            return;

        // Deactivate all TutoSteps
        foreach (TutoStep tutoStep in StepHolders[CurrentStep].GetComponents<TutoStep>())
        {
            tutoStep.OnDeactivate();
        }

        StepHolders[CurrentStep].gameObject.SetActive(false);
        StepHolders[CurrentStep].GetComponent<TutoCompleteCondition>().OnComplete -= OnCurrentStepCompleted;

        // Complete the tuto instance if it was the last step
        if (_isDebugVerbose) Debug.Log("[TutorialInstance] Completed Step  : " + CurrentStep + "/" + (StepHolders.Count - 1));
        if (CurrentStep >= StepHolders.Count - 1)
        {
            Complete();
            return;
        }
        ActivateStep(CurrentStep + 1);
    }

    void Complete()
    {
        if(_isDebugVerbose) Debug.Log("[TutorialInstance] Complete Tutorial : " + Id);
        TutorialManager.Instance.CompleteCurrentTutoInstance();
    }

    public void Debug_CompleteStep()
    {
        OnCurrentStepCompleted();
    }

    public void Debug_ActivatePreviousStep()
    {
        if(CurrentStep > 1)
            ActivateStep(CurrentStep - 1);
    }
}
