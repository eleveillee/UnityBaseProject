﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class UIGroupExample : UIGroup
{
    List<UICellExample> Examples = new List<UICellExample>();

    public void Setup()
    {
        Refresh();
    }

    protected override void OnRefresh()
    {
        //foreach (UnitInstance instance in _army.Units)
        {
            UICellExample cell = (UICellExample)AddCell();
            cell.Setup();
        }
    }
}
