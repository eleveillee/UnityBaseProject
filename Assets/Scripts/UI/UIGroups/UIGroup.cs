﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIGroup : MonoBehaviour
{
    [SerializeField] bool _isSelectable;
    [SerializeField] bool _selectFirstCellOnSetup;
    protected List<UICell> _cells = new List<UICell>();
    protected UICell _selectedCell;
    [SerializeField] UICell _prefabCell;
    public Action<UICell> OnCellClicked;

    public void SelectCell(int index)
    {
        if (_cells == null || _cells.Count <= index)
            return;

        OnCellClickBase(_cells[index]);
    }

    public void Refresh() //Todo: Remove delegates
    {
        for (int i = _cells.Count - 1; i >= 0; i--)
        {
            Destroy(_cells[i].gameObject);
        }
        _cells.Clear();
        
        OnRefresh();

        if (_selectFirstCellOnSetup)
            SelectCell(0);
    }

    public UICell AddCell()
    {
        UICell cell = Instantiate(_prefabCell, transform);
        //cellBuilding.Setup(bInstance, _city, true, bInstance == CurrentBuildingInstance);
        cell.OnCellClick += OnCellClickBase;
        _cells.Add(cell);
        return cell;
    }

    protected virtual void OnRefresh() { }

    protected virtual void OnCellClick(UICell cell)
    {

    }

    private void OnCellClickBase(UICell cell)
    {
        if(_isSelectable)
        {
            if (_selectedCell == null || cell != _selectedCell) //ToDo: Fix this
            {
                if (_selectedCell != null)
                    _selectedCell.ToggleSelect(false);

                _selectedCell = cell;
                _selectedCell.ToggleSelect(true);
            }
        }
        
        OnCellClick(cell);
        OnCellClicked.SafeInvoke(cell);
    }
}
