﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private bool _isDebugVerbose = false;
    public static UIManager Instance;

    [Header("UI Customs")]
    public UIOverlay Overlay;
    public UIDialog Dialog;
    public UIHideScreen HideScreen;
    public UISplash Splash;
    public bool IsShowingSplashScreen = true;
    

    [Header("UI Panels")]
    /*public UIPanelMain PanelMain;
    public UIPanelWorldSelect PanelWorldSelect;
    public UIPanelShop PanelShop;
    public UIPanelDebug PanelDebug;
    public UIPanelPause PanelPause;
    public UIPanelSettings PanelSettings;
    public UIPanelHelper PanelHelper;
    public UIPanelEndGame PanelEndGame;*/

    [Header("UI Floating Text")]
    [SerializeField] UIFloatingText _uiFloatingTextPrefab;
    [SerializeField] int _uiFloatingTextsLimit;
    List<UIFloatingText> _floatingTexts = new List<UIFloatingText>();

    [NonSerialized] public UIPanel CurrentPanel;

    // Panels
    List<UIPanel> GetAllPanels() { return transform.GetComponentsInChildren<UIPanel>().ToList(); }
    public void OpenBasePanel() {
        Debug.Log("OpenBasePanel");
        //PanelMain.Setup().Open();
    }
    public void CloseAllPanels() { GetAllPanels().ForEach(x => x.Close()); if (_isDebugVerbose) Debug.Log("[UIManager] CloseAllPanels"); }

    // Use this for initialization
    void Awake()
    {
        Instance = this;

        //HideScreen.gameObject.SetActive(false);
        //Dialog.Close();
        CloseAllPanels();
    }
    
    #region FloatingText
    public void SpawnFloatingText(string text, float duration = -1f) { SpawnFloatingText(text, Vector3.negativeInfinity, duration); }
    public void SpawnFloatingText(string text, Vector3 initialPosition, float duration = -1f) { SpawnFloatingText(text, initialPosition, Vector3.negativeInfinity, duration); }

    public void SpawnFloatingText(string text, Vector3 initialPosition, Vector3 offset, float duration = -1f)
    {
        UIFloatingText floatingText = Instantiate(_uiFloatingTextPrefab);
        floatingText.Setup(text, initialPosition, offset, duration);
        _floatingTexts.Add(floatingText);
        if (_floatingTexts.Count > _uiFloatingTextsLimit)
            DeleteFloatingText(_floatingTexts[0]);
    }

    public void DeleteFloatingText(UIFloatingText uiFloatingText)
    {
        _floatingTexts.Remove(uiFloatingText);
        LeanTween.cancel(uiFloatingText.gameObject);
        Destroy(uiFloatingText.gameObject);
    }
    #endregion
}