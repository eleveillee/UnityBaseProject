﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class UIPanel : MonoBehaviour
{
    protected bool _isOpen;
    public void Close()
    {
        UIManager.Instance.CurrentPanel = null;
        if(UIManager.Instance.HideScreen != null) UIManager.Instance.HideScreen.ChangeOpacity(0f, 0.5f);
        gameObject.SetActive(false);

        if (!_isOpen)
            return;

        _isOpen = false;
        OnClose();
    }

    public void Open(bool isCloseAll = true)
    {
        if (isCloseAll)
        {
            UIManager.Instance.CloseAllPanels();
        }
        
        if(UIManager.Instance.CurrentPanel == null)
            UIManager.Instance.HideScreen.ChangeOpacity(0.75f, 0.5f);

        UIManager.Instance.CurrentPanel = this;

        gameObject.SetActive(true);
        
        if (_isOpen)
            return;

        _isOpen = true;
        OnOpen();
    }

    protected virtual void OnOpen() { }
    protected virtual void OnClose() { }
}
