﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelExample : UIPanel
{
    public UIPanel Setup()
    {
        return this;
    }
    
    public void Refresh()
    {
    }
}

public enum PanelCitySub
{
    Building,
    Inventory,
    Workers
}