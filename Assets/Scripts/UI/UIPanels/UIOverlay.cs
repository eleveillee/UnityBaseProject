﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIOverlay : MonoBehaviour
{
	public void Show(bool isVisible)
    {
        gameObject.SetActive(isVisible);
    }
}
