﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UIDialog : MonoBehaviour
{
    [SerializeField] Text _headerText;
    [SerializeField] Text _dialogText;
    [SerializeField] Button _yesButton;
    [SerializeField] Button _noButton;
    Action _onClose;
    Action _onYes;
    Action _onNo;

    public UIDialog Setup(string text, string header = "", Action onClose = null, Action onYes = null, Action onNo = null)
    {
        _onClose = onClose;
        _onYes = onYes;
        _onNo = onNo;
        _yesButton.gameObject.SetActive(_onYes != null);
        _noButton.gameObject.SetActive(_onNo != null);
        _headerText.text = string.IsNullOrEmpty(header) ? "Message" : header;
        _dialogText.text = text;
        return this;
    }

    internal void UpdateText(StringBuilder _liveText)
    {
        _dialogText.text = _liveText.ToString();
    }

    public void Open()
    {
        if(gameObject.activeInHierarchy)
        {
            Debug.LogWarning("Trying to open an already open UIDialog. Not Permitted. New Header : " + _headerText.text);
            return;
        }

        //UIManager.Instance.HideScreen.ChangeOpacity(0.75f, 0.5f);
        gameObject.SetActive(true);
    }

    public void Close()
    {
        //if(UIManager.Instance.CurrentPanel == null)
            //UIManager.Instance.HideScreen.ChangeOpacity(0f, 0.5f);

        _onClose.SafeInvoke();
        if(gameObject != null)
            gameObject.SetActive(false);
    }

    public void zClick_OnYes()
    {
        Debug.Log("YES");
        _onYes.SafeInvoke();
        Close();
    }

    public void zClick_OnNo()
    {
        _onNo.SafeInvoke();
        Close();
    }

    private void OnDestroy()
    {
        LeanTween.cancel(gameObject);
    }
}
