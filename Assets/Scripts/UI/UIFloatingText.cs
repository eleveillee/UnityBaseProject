﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIFloatingText : MonoBehaviour {

    [SerializeField] TextMeshProUGUI _text;
    [SerializeField] Vector2 _defaultOffset;
    [SerializeField] float _defaultDuration;

    float _maxHp;
    float _timeDeath;
	public void Setup(string text, Vector3 initialPosition, Vector3 offset, float duration = -1)
    {
        if (duration <= 0f)
            duration = _defaultDuration;

        if (Mathf.Abs(offset.x) > 10000f) // Use Default
            offset = _defaultOffset;

        /*if (Mathf.Abs(initialPosition.x) > 10000f) // Use Default
            initialPosition = Player.Instance.GetFloatingTextPosition();*/

        transform.position = initialPosition;
        _timeDeath = Time.time + duration;
        _text.text = text;
        StartFade(duration, offset);
    }

    private void Update()
    {
        if (_timeDeath > 0 && Time.time > _timeDeath)
            Delete();
    }

    void StartFade(float duration, Vector3 offset)
    {
        LeanTween.moveLocal(gameObject, transform.position + offset, duration); // ;.setOnComplete(() => Destroy(gameObject));
        //LeanTween.color(_text.rectTransform, new Color(0f, 0f, 0f, 0f), duration);
    }

    private void Delete()
    {
        UIManager.Instance.DeleteFloatingText(this);
    }
}
