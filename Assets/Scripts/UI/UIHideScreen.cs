﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UIHideScreen : MonoBehaviour {

    bool _isDebugVerbose = false;
    [SerializeField] Image _image;
    
    public void ChangeOpacity(float opacity, float duration = 0f)
    {
        if (_isDebugVerbose) Debug.Log("[UIHideScreen] Opacity : " + _image.color.a + " -> " + opacity + " (" + duration + "sec)");
        Color target = new Color(_image.color.r, _image.color.g, _image.color.b, opacity);

        if (duration <= 0f)
            _image.color = target;
        else
            LeanTween.color(_image.rectTransform, target, duration).setEaseOutCubic();

        gameObject.SetActive(true);
        if (_isDebugVerbose) Debug.Log("[UIHideScreen] Opacity2 : " + _image.color.a);
    }

    public void Show(bool isVisible)
    {
        if (_isDebugVerbose) Debug.Log("[UIHideScreen] Show : " + isVisible);
        gameObject.SetActive(isVisible);
    }
}
