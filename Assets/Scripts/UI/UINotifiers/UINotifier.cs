﻿using UnityEngine;
using System.Collections;

public abstract class UINotifier : MonoBehaviour
{
    // Bump
    float initialBumpDelay = 0.75f;
    float bumpDelay = 2.5f;
    float bumpDuration = 0.5f;
    float bumpMaxScale = 1.15f;

    void Start()
    {
        Test();
        LeanTween.delayedCall(gameObject, initialBumpDelay, TweenFocus);
        //TweenFocus();
    }
    
    void TweenFocus()
    {
        TweenHelper.BumpIn(gameObject, bumpDuration, bumpMaxScale, false, () => LeanTween.delayedCall(gameObject,bumpDelay, TweenFocus));
    }
    
    // Update is called once per frame
    public void Toggle(bool isEnabled)
    {
       gameObject.SetActive(isEnabled);
    }

    public void Test()
    {
        Toggle(IsTrue());
    }

    protected abstract bool IsTrue();

    private void OnDestroy()
    {
        transform.localScale = Vector3.one;
        LeanTween.cancel(gameObject);
    }
}
