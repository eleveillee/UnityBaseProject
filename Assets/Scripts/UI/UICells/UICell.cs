﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICell : MonoBehaviour {

    [Header("Selection")]
    [SerializeField] GameObject _selectedGameObject;
    [SerializeField] Image _selectionBackground;
    [SerializeField] Color _selectedColor;
    Color _defaultColor;


    public Action<UICell> OnCellClick;

    private Button _buttonVar;
    protected Button _button { get { if (_buttonVar == null) _buttonVar = GetComponent<Button>(); return _buttonVar; } }

    /*private void Awake()
    {
        _defaultColor = _selectionBackground.color;
    }*/

    // Use this for initialization
    public void zClick_Cell ()
    {
        OnCellClick.SafeInvoke(this);
    }

    protected virtual void OnRefresh() { }
    public void Refresh()
    {
        OnRefresh();
    }

    public void ToggleSelect(bool isSelected)
    {
        //Debug.Log("ToggleSelected : " + isSelected + " - " + _defaultColor);
        _selectionBackground.color = isSelected ? _selectedColor : Color.white;// _defaultColor;
        if(_selectedGameObject != null)
            _selectedGameObject.SetActive(isSelected);
    }
}
