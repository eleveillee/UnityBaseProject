using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataCondition_Bool : DataCondition
{
    [SerializeField] bool _isTrue;
    
    public override bool IsTrue()
    {
        return _isTrue;
    }   
}