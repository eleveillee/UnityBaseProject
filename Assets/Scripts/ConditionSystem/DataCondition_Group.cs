using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataCondition_Group : DataCondition
{
    [SerializeField] EnumOperator _operator;
    [SerializeField] List<DataCondition> _conditions = new List<DataCondition>();

    public override bool IsTrue()
    {
        bool isCompleted = _operator == EnumOperator.AND;
        foreach (DataCondition condition in _conditions)
        {
            if (_operator == EnumOperator.AND)
                isCompleted &= condition.IsTrue();
            else
                isCompleted |= condition.IsTrue();
        }

        return isCompleted;
    }   
}