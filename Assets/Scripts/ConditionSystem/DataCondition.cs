using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Lean;

// Base condition to complete a tuto step
[Serializable]
public abstract class DataCondition : ScriptableObject
{
    public bool IsTrackingCompletion;
    public Action OnComplete;
    public abstract bool IsTrue();

    public virtual string GetDescription()
    {
        return "UndefinedName_DataCondition_Base";
    }
    
    //public EnumConditionType Type;
    //public List<string> Values = new List<string>();

    //public Condition()
    //{
    //    OnInitialize();
    //}

    //~Condition()
    //{
    //    OnDisable();
    //}

    //public virtual void OnInitialize(){}
    //public virtual void OnDisable(){}

    //protected bool _isComplete;
    //public bool IsComplete
    //{
    //    get { return (_isComplete || Type == EnumConditionType.True) && Type != EnumConditionType.False; }
    //    set { _isComplete = value;  }
    //}
}

public enum EnumOperator
{
    AND,
    OR
}